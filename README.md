# Documentation Starter Website

This is a website for publishing online documentation.

- Content Management System: [Pico CMS](https://picocms.org) version 2.1.4
- Search: [Pico-Search](https://github.com/PontusHorn/Pico-Search "PontusHorn Pico-Search in GitHub") plugin by PontusHorn.
- Theme: based on the [CoderDocs](https://themes.3rdwavemedia.com/demo/bs5/coderdocs/index.html) Bootstrap 5 Documentation Template by [Xiaoying Riley](http://themes.3rdwavemedia.com/).

## Installation

- [Requirements](https://picocms.org/docs/#install "Pico Installation Documentation"): PHP 5.3.6+ and the PHP extensions 'dom' and 'mbstring' to be enabled.
- Instructions: Copy the repository to the root of the web folder.

## Usage

- Usage requirements and other documentation for the inputs are available at the links above.
- Documentation for the managing the content based on the theme is in the **~/content** folder.
- The documentation forms starter content for the website, demonstrating its use.
